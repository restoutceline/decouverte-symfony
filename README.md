# Mise en place

### Dockerfile

```
# Based from a PHP 8.0 VERSION with APACHE SERVER
FROM php:8.0-apache

# Install Git, Curl, Wget and Nano // And PDO/MYSQLI
RUN apt update -y && apt install -y git curl wget nano
RUN docker-php-ext-install pdo pdo_mysql mysqli

# Starting Directory that will contain the folder of our project
WORKDIR /var/www/html

# Install Composer
RUN curl -Ss https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer
RUN chmod +x /usr/local/bin/composer

# Git Personal Informations
RUN git config --global user.email "myname@gmail.com"
RUN git config --global user.name "myname"

# Install Symfony
RUN echo 'deb [trusted=yes] https://repo.symfony.com/apt/ /' | tee /etc/apt/sources.list.d/symfony-cli.list
RUN apt update
RUN apt install symfony-cli
RUN symfony new --webapp app

# Directory that will contain our project
WORKDIR /var/www/html/app

## Install Webpack
RUN composer require symfony/webpack-encore-bundle 
RUN apt update && apt install npm -y
RUN npm install @symfony/webpack-encore --save-dev
RUN npm run dev
```

### Etapes de creation de l'image:

- Ouvrir kali
- cd ~
- mkdir symfo-image
- cd symfo-image/
- nano Dockerfile ((Si vous avez pas nano ------> apt update && apt install nano -y  ))
- copy paste Dockerfile
- N'Oubliez pas de changer les informations personnelles de GIT !!!!!!
- enregistrer le dockerfile
- docker build -t be_symfony:1.0 .


### Ligne de commande terminal Linux pour créer le container

```
docker run -dti --name <nom-du-container> be_symfony:1.0
```
