<?php

namespace App\Entity;

use App\Repository\InstrumentisteRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: InstrumentisteRepository::class)]
class Instrumentiste
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $personne;

    #[ORM\Column(type: 'string', length: 255)]
    private $instrument;

    #[ORM\Column(type: 'string', length: 255)]
    private $note;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPersonne(): ?string
    {
        return $this->personne;
    }

    public function setPersonne(string $personne): self
    {
        $this->personne = $personne;

        return $this;
    }

    public function getInstrument(): ?string
    {
        return $this->instrument;
    }

    public function setInstrument(string $instrument): self
    {
        $this->instrument = $instrument;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }
}
