<?php

namespace App\Entity;

use App\Repository\OrchestreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrchestreRepository::class)]
class Orchestre
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $nom;

    #[ORM\Column(type: 'string', length: 255)]
    private $directeur;

    #[ORM\Column(type: 'string', length: 255)]
    private $musicien;

    #[ORM\OneToMany(mappedBy: 'orchestre', targetEntity: Concert::class)]
    private $concert;

    public function __construct()
    {
        $this->concert = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDirecteur(): ?string
    {
        return $this->directeur;
    }

    public function setDirecteur(string $directeur): self
    {
        $this->directeur = $directeur;

        return $this;
    }

    public function getMusicien(): ?string
    {
        return $this->musicien;
    }

    public function setMusicien(string $musicien): self
    {
        $this->musicien = $musicien;

        return $this;
    }

    /**
     * @return Collection|Concert[]
     */
    public function getConcert(): Collection
    {
        return $this->concert;
    }

    public function addConcert(Concert $concert): self
    {
        if (!$this->concert->contains($concert)) {
            $this->concert[] = $concert;
            $concert->setOrchestre($this);
        }

        return $this;
    }

    public function removeConcert(Concert $concert): self
    {
        if ($this->concert->removeElement($concert)) {
            // set the owning side to null (unless already changed)
            if ($concert->getOrchestre() === $this) {
                $concert->setOrchestre(null);
            }
        }

        return $this;
    }
}
