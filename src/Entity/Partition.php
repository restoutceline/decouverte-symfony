<?php

namespace App\Entity;

use App\Repository\PartitionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PartitionRepository::class)]
#[ORM\Table(name: '`partition`')]
class Partition
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $annee;

    #[ORM\Column(type: 'integer')]
    private $opus;

    #[ORM\Column(type: 'string', length: 255)]
    private $titre;


    #[ORM\Column(type: 'string', length: 255)]
    private $instrument;

    #[ORM\Column(type: 'integer')]
    private $duree;

    #[ORM\Column(type: 'string', length: 255)]
    private $style;

    #[ORM\ManyToOne(targetEntity: Compositeur::class, inversedBy: 'partitions')]
    #[ORM\JoinColumn(nullable: false)]
    private $compositeur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    public function getOpus(): ?int
    {
        return $this->opus;
    }

    public function setOpus(int $opus): self
    {
        $this->opus = $opus;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getCompositeur(): ?string
    {
        return $this->compositeur;
    }

    public function setCompositeur(string $compositeur): self
    {
        $this->compositeur = $compositeur;

        return $this;
    }

    public function getInstrument(): ?string
    {
        return $this->instrument;
    }

    public function setInstrument(string $instrument): self
    {
        $this->instrument = $instrument;

        return $this;
    }

    public function getDuree(): ?int
    {
        return $this->duree;
    }

    public function setDuree(int $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getStyle(): ?string
    {
        return $this->style;
    }

    public function setStyle(string $style): self
    {
        $this->style = $style;

        return $this;
    }
}
