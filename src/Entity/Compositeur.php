<?php

namespace App\Entity;

use App\Repository\CompositeurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CompositeurRepository::class)]
class Compositeur
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $personne;

    #[ORM\Column(type: 'string', length: 255)]
    private $instrument;

    #[ORM\Column(type: 'string', length: 255)]
    private $link;

    #[ORM\OneToMany(mappedBy: 'compositeur', targetEntity: Partition::class, orphanRemoval: true)]
    private $partitions;

    public function __construct()
    {
        $this->partitions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPersonne(): ?string
    {
        return $this->personne;
    }

    public function setPersonne(string $personne): self
    {
        $this->personne = $personne;

        return $this;
    }

    public function getInstrument(): ?string
    {
        return $this->instrument;
    }

    public function setInstrument(string $instrument): self
    {
        $this->instrument = $instrument;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return Collection|Partition[]
     */
    public function getPartitions(): Collection
    {
        return $this->partitions;
    }

    public function addPartition(Partition $partition): self
    {
        if (!$this->partitions->contains($partition)) {
            $this->partitions[] = $partition;
            $partition->setCompositeur($this);
        }

        return $this;
    }

    public function removePartition(Partition $partition): self
    {
        if ($this->partitions->removeElement($partition)) {
            // set the owning side to null (unless already changed)
            if ($partition->getCompositeur() === $this) {
                $partition->setCompositeur(null);
            }
        }

        return $this;
    }
}
