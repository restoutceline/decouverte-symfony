<?php

namespace App\Entity;

use App\Repository\ConcertRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ConcertRepository::class)]
class Concert
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $date;

    #[ORM\Column(type: 'integer')]
    private $heure;


    #[ORM\Column(type: 'string', length: 255)]
    private $programme;

    #[ORM\Column(type: 'integer', length: 255)]
    private $jauge;

    #[ORM\ManyToOne(targetEntity: Orchestre::class, inversedBy: 'concert')]
    private $orchestre;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?int
    {
        return $this->date;
    }

    public function setDate(int $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getHeure(): ?int
    {
        return $this->heure;
    }

    public function setHeure(int $heure): self
    {
        $this->heure = $heure;

        return $this;
    }

    public function getOrchestre(): ?string
    {
        return $this->orchestre;
    }

    public function setOrchestre(string $orchestre): self
    {
        $this->orchestre = $orchestre;

        return $this;
    }

    public function getProgramme(): ?string
    {
        return $this->programme;
    }

    public function setProgramme(string $programme): self
    {
        $this->programme = $programme;

        return $this;
    }

    public function getJauge(): ?string
    {
        return $this->jauge;
    }

    public function setJauge(string $jauge): self
    {
        $this->jauge = $jauge;

        return $this;
    }
}
