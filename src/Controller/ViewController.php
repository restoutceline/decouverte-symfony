<?php

namespace App\Controller;

use App\Entity\Personne;
use App\Repository\PersonneRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\PersonneType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class ViewController extends AbstractController
{
    #[Route('/view', name: 'view')]

    public function index(PersonneRepository $repo): Response
    {
        $personnes = $repo->getAll();

        if (!$personnes) {
            throw $this->createNotFoundException(
                'pas de personne '
            );
        }

        $context = [
            'personnes' => $personnes
        ];

        return $this->render('view/index.html.twig', $context);
    }


    #[Route('/formulaire', name: 'formulaire')]

    public function form(Request $request, EntityManagerInterface $entityManager): Response
    {
        $personne = new Personne();

        $form = $this->createForm(PersonneType::class, $personne);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $entityManager->persist($personne);
            $entityManager->flush();
        }
        return $this->render('view/formulaire.html.twig', ['form'=>$form->createView()]);
    }


}
